package com.M1graphQL.M1graphQL;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class M1graphQlApplication {

	public static void main(String[] args) {
		SpringApplication.run(M1graphQlApplication.class, args);
	}
}
