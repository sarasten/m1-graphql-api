package com.M1graphQL.M1graphQL.entity;
import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String Title, Description, Genre, PosterUrl;

    public Movie(String title, String description, String genre, String posterUrl){
        this.Title = title;
        this.Description = description;
        this.Genre = genre;
        this.PosterUrl = posterUrl;
    }

    public Movie() {

    }
}