package com.M1graphQL.M1graphQL.exceptions;

import com.fasterxml.jackson.annotation.JsonInclude;
import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MovieNotFoundException extends RuntimeException implements GraphQLError {


    public MovieNotFoundException(String message) {
        super(message);
    }

    @Override
    public Throwable fillInStackTrace() {
        return this;
    }

    @Override
    public List<Object> getPath() {
        return null;
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorType getErrorType() {
        return null;
    }
}