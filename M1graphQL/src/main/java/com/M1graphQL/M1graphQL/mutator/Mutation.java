package com.M1graphQL.M1graphQL.mutator;

import com.M1graphQL.M1graphQL.entity.Movie;
import com.M1graphQL.M1graphQL.exceptions.MovieNotFoundException;
import com.M1graphQL.M1graphQL.service.MovieService;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.stereotype.Component;

@Component
public class Mutation implements GraphQLMutationResolver {

    private final MovieService movieService;

    public Mutation(MovieService movieService){
        this.movieService = movieService;
    }

    public Movie addMovie(String title, String description, String genre, String posterUrl){
        if (title != null) {
            return movieService.addMovie(title, description, genre, posterUrl);
        }
        else throw new MovieNotFoundException("You need to specify a title.");
    }

    public boolean deleteMovie(Long id){
        if (id != null) {
            return movieService.deleteMovie(id);
        }
        else throw new MovieNotFoundException("You need to specify an ID.");
    }

    public Movie updateMovie(String newTitle, String newGenre, String newDesc, String newPosterUrl, Long id) {
        if (id != null) {
            return movieService.updateMovie(newTitle, newGenre, newDesc, newPosterUrl, id);
        }
        else throw new MovieNotFoundException("You need to specify an ID.");
    }
}