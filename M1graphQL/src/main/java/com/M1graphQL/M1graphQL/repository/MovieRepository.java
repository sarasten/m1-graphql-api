package com.M1graphQL.M1graphQL.repository;
import com.M1graphQL.M1graphQL.entity.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

    @Query(value  = " SELECT * FROM movie m" + " WHERE m.genre = :searchGenre", nativeQuery = true)
    List<Movie> findMoviesByGenre(@Param("searchGenre") String searchGenre);
}
