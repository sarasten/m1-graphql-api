package com.M1graphQL.M1graphQL.resolver;

import com.M1graphQL.M1graphQL.entity.Movie;
import com.M1graphQL.M1graphQL.exceptions.MovieNotFoundException;
import com.M1graphQL.M1graphQL.service.MovieService;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.stereotype.Component;

@Component
public class Query implements GraphQLQueryResolver {

    private final MovieService movieService;

    public Query(MovieService movieService){
        this.movieService = movieService;
    }

    public Iterable<Movie> findAllMovies(){
        return movieService.findAllMovies();
    }

    public Movie findMovie(Long id){
        if (id != null) {
            return movieService.findMovie(id);
        }
        else throw new MovieNotFoundException("You need to specify an ID.");
    }

    public Iterable<Movie> findMoviesByGenre(String genre){
        if (genre != null) {
            return movieService.findMoviesByGenre(genre);
        }
        else throw new MovieNotFoundException("You need to specify a genre.");
    }

    public int countMovies(){
        return movieService.countMovies();
    }
}
