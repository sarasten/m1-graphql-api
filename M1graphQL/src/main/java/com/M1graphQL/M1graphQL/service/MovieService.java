package com.M1graphQL.M1graphQL.service;

import com.M1graphQL.M1graphQL.entity.Movie;
import com.M1graphQL.M1graphQL.exceptions.MovieNotFoundException;
import com.M1graphQL.M1graphQL.repository.MovieRepository;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class MovieService {
    private final MovieRepository movieRepository;

    public MovieService(MovieRepository movieRepository){
        this.movieRepository = movieRepository;
    }

    //    Query methods    //

    public Iterable<Movie> findAllMovies(){
        var movies = movieRepository.findAll();

        if (movies.isEmpty())
            throw new MovieNotFoundException("There are no movies in the database.");
        else return movies;
    }

    public Movie findMovie(Long id){
        var movie = movieRepository.findById(id).orElse(null);

        if (movie == null) {
            throw new MovieNotFoundException("No movie with id " +  id + " found.");
        }
        else return movie;
    }

    public List<Movie> findMoviesByGenre(String genre){
        genre = genre.substring(0, 1).toUpperCase()
                + genre.substring(1).toLowerCase();

        var movies = movieRepository.findMoviesByGenre(genre);

        if (movies.isEmpty())
            throw new MovieNotFoundException("There are no movies with genre " + genre);
        else return movies;
    }

    public int countMovies(){
        var movieCount = movieRepository.count();
        return Math.toIntExact(movieCount);
    }

    //    Mutation methods    //

    public Movie addMovie(String title, String description, String genre, String posterUrl){

        genre = genre.substring(0, 1).toUpperCase()
                + genre.substring(1).toLowerCase();

        Movie newMovie = new Movie(title, description, genre, posterUrl);
        movieRepository.save(newMovie);
        return newMovie;
    }

    public boolean deleteMovie(Long id){
        if (movieRepository.findById(id).isPresent())
        {
            movieRepository.deleteById(id);
            return true;
        }
        else throw new MovieNotFoundException("No movie with id " + id + " found.");
    }

    public Movie updateMovie(String newTitle, String newGenre, String newDesc, String newPosterUrl, Long id){
        Optional<Movie> optMovie = movieRepository.findById(id);

        if (optMovie.isPresent())
        {
            Movie movie = optMovie.get();
            if (newTitle != null && !newTitle.isEmpty()){
                movie.setTitle(newTitle);
            }
            if (newGenre != null && !newGenre.isEmpty()){
                movie.setGenre(newGenre);
            }
            if (newDesc != null && !newDesc.isEmpty()) {
                movie.setDescription(newDesc);
            }
            if (newPosterUrl != null && !newPosterUrl.isEmpty()) {
                movie.setPosterUrl(newPosterUrl);
            }
            movieRepository.save(movie);
            return movie;
        } else
        {
            throw new MovieNotFoundException("No movie with id " + id + " found.");
        }
    }
}
